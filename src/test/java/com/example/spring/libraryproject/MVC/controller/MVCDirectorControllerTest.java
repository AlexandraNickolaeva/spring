package com.example.spring.libraryproject.MVC.controller;

import com.example.spring.libraryproject.dto.DirectorDTO;
import com.example.spring.libraryproject.service.DirectorService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Slf4j
@Transactional
@Rollback(value = false)
public class MVCDirectorControllerTest extends CommonTestMVCTest {
    private final DirectorDTO directorDTO = new DirectorDTO("MVC_TestDirectorsFio", "Test Description", new HashSet<>(), false);
    private final DirectorDTO directorDTOUpdated = new DirectorDTO("MVC_TestDirectorsFio_UPDATED", "Test Description", new HashSet<>(), false);
    @Autowired
    private DirectorService directorService;

    /**
     * Метод, тестирующий просмотр всех режиссеров через MVC-контроллер.
     * Ожидаем, что результат ответа будет просто любой 200 статус.
     * Проверяем, что view, на которое нас перенаправит контроллер, при удачном вызове - это как раз показ всех режиссеров
     * Так-же, ожидаем, что в модели будет атрибут directors.
     *
     * @throws Exception - любая ошибка
     */
    @Test
    @DisplayName("Просмотр, тестирование 'directors/getAll()'")
    @Order(0)
    @WithMockUser(username = "admin", roles = "ADMIN", password = "admin")
    @Override
    protected void listAll() throws Exception {
        log.info("Тест начат успешно");
        mvc.perform(get("/directors")
                        .param("page", "1")
                        .param("size", "5")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("directors/viewAllDirectors"))
                .andExpect(model().attributeExists("directors"));
        log.info("Тест закончен успешно");
    }

    /**
     * Метод, тестирующий создание режиссера через MVC-контроллер.
     * Авторизуемся под пользователем admin (можно выбрать любого),
     * создаем шаблон данных и вызываем MVC-контроллер с соответствующим маппингом и методом.
     * flashAttr - используется, чтобы передать ModelAttribute в мето644\49д контроллера
     * Ожидаем, что будет статус redirect (как у нас в контроллере) при успешном создании
     *
     * @throws Exception - любая ошибка
     */
    @Test
    @DisplayName("Создание, тестирование 'directors/add'")
    @Order(1)
    @WithMockUser(username = "admin", roles = "ADMIN", password = "admin")
    @Override
    protected void createObject() throws Exception {
        log.info("Тест по созданию начат успешно");
        mvc.perform(post("/directors/add")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .flashAttr("directorForm", directorDTO)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .with(csrf()))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/directors"))
                .andExpect(redirectedUrlTemplate("/directors"))
                .andExpect(redirectedUrl("/directors"));
        log.info("Тест по созданию закончен успешно");
    }

    @Order(2)
    @Test
    @DisplayName("Обновление, тестирование 'directors/update'")
    @WithMockUser(username = "admin", roles = "ADMIN", password = "admin")
    @Override
    protected void updateObject() throws Exception {
        log.info("Тест по обновлению начат успешно");
        PageRequest pageRequest = PageRequest.of(0, 5, Sort.by(Sort.Direction.ASC, "directorsFio"));
        DirectorDTO foundAuthorForUpdate = directorService.searchDirectors(directorDTO.getDirectorsFio(), pageRequest).getContent().get(0);
        foundAuthorForUpdate.setDirectorsFio(directorDTOUpdated.getDirectorsFio());
        mvc.perform(post("/directors/update")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .flashAttr("directorForm", foundAuthorForUpdate)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                )
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/directors"))
                .andExpect(redirectedUrl("/directors"));
        log.info("Тест по обновлению закончен успешно");
    }

    @Test
    @Order(3)
    @Override
    @DisplayName("Удаление, тестирование 'directors/delete'")
    @WithMockUser(username = "admin", roles = "ADMIN", password = "admin")
    protected void deleteObject() throws Exception {
        log.info("Тест по удалению начат успешно");
        mvc.perform(MockMvcRequestBuilders.get("/directors/delete/{id}", "1"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/directors"));
        log.info("Тест по удалению закончен успешно");
    }
}


