package com.example.spring.dbexample.dao;

import com.example.spring.dbexample.model.User;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.sql.*;

@Component
@Scope("prototype")
public class UserDAO {
    private final Connection connection;
    private final BookDAO bookDAO;

    public UserDAO(Connection connection, BookDAO bookDAO) {
        this.connection = connection;
        this.bookDAO = bookDAO;
    }

    public void insertUser(User u) throws SQLException {
        //String sql = "INSERT INTO Products (ProductName, Price) Values (?, ?)";
        PreparedStatement selectQuery = connection.prepareStatement("insert into client" + "(surname, name_client, birthday, mobile, mail, books_client)" + "values(?, ?, ?, ?,?,?)");
        selectQuery.setString(1, u.getSurname());
        selectQuery.setString(2, u.getName_client());
        selectQuery.setString(3, u.getBirthday());
        selectQuery.setString(4, u.getMobile());
        selectQuery.setString(5, u.getMail());
        selectQuery.setString(6, u.getBooks_client());

        int rows = selectQuery.executeUpdate();
        System.out.printf("%d rows \n", rows);
    }

    public void findBooks(String mobile) throws SQLException {
        PreparedStatement selectQuery = connection.prepareStatement("select books_client from client where mobile = ?");
        selectQuery.setString(1, mobile);
        ResultSet resultSet = selectQuery.executeQuery();
        while (resultSet.next()) {
            String result = resultSet.getString("books_client");
            String[] title = result.split(", ");
            for (String qwe : title)
                bookDAO.infoBooks(qwe);
        }
    }
}