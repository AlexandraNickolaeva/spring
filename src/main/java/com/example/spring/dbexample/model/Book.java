package com.example.spring.dbexample.model;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString

public class Book {
    @Setter(AccessLevel.NONE)
    private Integer bookId;

    private String bookTitle;
    private String bookAuthor;
}
