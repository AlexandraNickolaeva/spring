package com.example.spring.dbexample.model;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class User {
    @Setter(AccessLevel.NONE)
    private Integer clientId;

    private String surname;
    private String name_client;
    private String birthday;
    private String mobile;
    private String mail;
    private String books_client;

    public User(String surnam, String nam, String day, String mobil, String mai, String lis) {
        this.surname = surnam;
        this.name_client = nam;
        this.birthday = day;
        this.mobile = mobil;
        this.mail = mai;
        this.books_client = lis;
    }
}
