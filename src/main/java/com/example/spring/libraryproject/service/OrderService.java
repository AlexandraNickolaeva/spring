package com.example.spring.libraryproject.service;

import com.example.spring.libraryproject.dto.FilmDTO;
import com.example.spring.libraryproject.dto.OrderDTO;
import com.example.spring.libraryproject.mapper.OrderMapper;
import com.example.spring.libraryproject.model.Order;
import com.example.spring.libraryproject.repository.OrderRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class OrderService
        extends GenericService<Order, OrderDTO>{
    private final FilmService filmService;
    private final OrderMapper orderMapper;
    private final OrderRepository orderRepository;

    protected OrderService(OrderRepository orderRepository, OrderMapper orderMapper,
                           FilmService filmService) {
        super(orderRepository, orderMapper);
        this.filmService = filmService;
        this.orderMapper = orderMapper;
        this.orderRepository = orderRepository;
    }

    public Page<OrderDTO> listUserRentFilms(final Long id,
                                            final Pageable pageable) {
        Page<Order> objects = orderRepository.getOrderByUserId(id, pageable);
        List<OrderDTO> results = orderMapper.toDTOs(objects.getContent());
        return new PageImpl<>(results, pageable, objects.getTotalElements());
    }

    public OrderDTO rentFilm(OrderDTO rentFilmDTO) {
        FilmDTO filmDTO = filmService.getOne(rentFilmDTO.getFilmId());
        filmService.update(filmDTO);
        long rentPeriod = rentFilmDTO.getRentPeriod() != null ? rentFilmDTO.getRentPeriod() : 14L;
        rentFilmDTO.setRentDate(LocalDateTime.now());
        rentFilmDTO.setPurchase(false);
        rentFilmDTO.setRentPeriod((int) rentPeriod);
        return mapper.toDTO(repository.save(mapper.toEntity(rentFilmDTO)));
    }

    public void returnFilm(final Long id) {
        OrderDTO orderDTO = getOne(id);
        orderDTO.setPurchase(true);
        FilmDTO filmDTO = orderDTO.getFilmDTO();
        update(orderDTO);
        filmService.update(filmDTO);
    }
}
