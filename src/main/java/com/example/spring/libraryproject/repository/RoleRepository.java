package com.example.spring.libraryproject.repository;

import com.example.spring.libraryproject.model.Role;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends GenericRepository<Role> {
}
