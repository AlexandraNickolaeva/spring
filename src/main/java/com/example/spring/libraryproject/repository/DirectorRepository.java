package com.example.spring.libraryproject.repository;

import com.example.spring.libraryproject.model.Director;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public interface DirectorRepository extends GenericRepository<Director> {
    Page<Director> findAllByDirectorsFioContainsIgnoreCase(String fio, Pageable pageable);
}
