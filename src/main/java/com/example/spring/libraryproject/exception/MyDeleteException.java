package com.example.spring.libraryproject.exception;

public class MyDeleteException extends Exception{
    public MyDeleteException(String message) {
        super(message);
    }
}
