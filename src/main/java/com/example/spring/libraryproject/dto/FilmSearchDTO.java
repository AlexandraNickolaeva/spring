package com.example.spring.libraryproject.dto;

import com.example.spring.libraryproject.model.Genre;
import groovy.transform.ToString;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ToString
public class FilmSearchDTO {
    private String filmTitle;
    private String directorsFio;
    private Genre genre;
}

