package com.example.spring.libraryproject.dto;

import com.example.spring.libraryproject.model.Genre;
import lombok.ToString;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class FilmDTO extends GenericDTO {
    private String filmTitle;
    private String premierYear;
    private String country;
    private Genre genre;
    private Set<Long> directorsIds;
    private boolean isDeleted;

}
