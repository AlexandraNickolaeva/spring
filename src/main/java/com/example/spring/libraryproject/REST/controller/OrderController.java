package com.example.spring.libraryproject.REST.controller;

import com.example.spring.libraryproject.dto.OrderDTO;
import com.example.spring.libraryproject.model.Order;
import com.example.spring.libraryproject.service.OrderService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/orders")
@Tag(name = "Заказы",
        description = "Контроллер для работы с заказами в фильмотеке")
@SecurityRequirement(name = "Bearer Authentication")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class OrderController extends GenericController<Order, OrderDTO> {
    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        super(orderService);
        this.orderService = orderService;
    }
}
