package com.example.spring.libraryproject.MVC.controller;

import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@Hidden
public class MainController {
    //http://localhost:9090/

    @GetMapping("/")
    public String index() {
        return "index";
    }
}
